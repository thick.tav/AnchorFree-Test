package com.tunebrains.anchorfreetest.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Alexandr Timoshenko <thick.tav@gmail.com> on 4/10/15.
 */
public class Prefs {
    public static final String SERVICE_WORKING = "service_working";
    private SharedPreferences mPrefs;

    public Prefs(Context pContext) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(pContext);
    }

    public boolean isServiceWorking() {
        return mPrefs.getBoolean(SERVICE_WORKING, false);
    }

    public void serviceStart() {
        mPrefs.edit().putBoolean(SERVICE_WORKING, true).apply();
    }

    public void serviceStop() {
        mPrefs.edit().putBoolean(SERVICE_WORKING, false).apply();
    }

    public void clear() {
        mPrefs = null;
    }
}
