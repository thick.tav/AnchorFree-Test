package com.tunebrains.anchorfreetest.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

/**
 * Created by Alexandr Timoshenko <thick.tav@gmail.com> on 4/10/15.
 */
public class DownloadsDB extends SQLiteOpenHelper {
    public static final String AUTHORITY = "com.tunebrains.anchorfreetest";
    public static final String CONTENT_AUTHORITY_SLASH = "content://" + AUTHORITY + "/";
    private static final String DB_NAME = "downloads.db";
    private static final int DB_VERSION = 1;
    private final String CREATE_DOWNLOADS_TABLE = "create table downloads(" +
            "_id INTEGER PRIMARY KEY," +
            "url text," +
            "dest_path text," +
            "total int default 0," +
            "downloaded int default 0" +
            ");";

    DownloadsDB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase pSQLiteDatabase) {
        pSQLiteDatabase.execSQL(CREATE_DOWNLOADS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase pSQLiteDatabase, int i, int i2) {

    }

    public static class DownloadColumns {
        public static final String _ID = "_id";
        public static final String URL = "url";
        public static final String TOTAL = "total";
        public static final String DOWNLOADED = "downloaded";
        public static final String DEST_PATH = "dest_path";

        public static Uri getContentUri() {
            return Uri.parse(CONTENT_AUTHORITY_SLASH + "external" +
                    "/downloads/");
        }

        public static Uri getItemUri() {
            return Uri.parse(CONTENT_AUTHORITY_SLASH + "external" +
                    "/download/");
        }


    }
}
