package com.tunebrains.anchorfreetest.data;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

/**
 * Created by Alexandr Timoshenko <thick.tav@gmail.com> on 4/11/15.
 */
public class DBHelper {
    public static void addDownload(Context pContext, String pUrl) {
        ContentResolver lContentResolver = pContext.getContentResolver();
        ContentValues lContentValues = new ContentValues();
        lContentValues.put(DownloadsDB.DownloadColumns.URL, pUrl);
        lContentResolver.insert(DownloadsDB.DownloadColumns.getContentUri(), lContentValues);
    }

    public static Download getDownloadFromDB(Context pContext, Uri pUri) {
        Cursor c = null;
        Download d = null;
        try {
            c = pContext.getContentResolver().query(pUri, null, DownloadsDB.DownloadColumns._ID + "=?", new String[]{pUri.getLastPathSegment()}, null);
            c.moveToFirst();
            d = downloadFromCursor(c);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null)
                c.close();
        }
        return d;
    }

    private static Download downloadFromCursor(Cursor c) {
        Download d = new Download();
        d.setID(c.getInt(c.getColumnIndex(DownloadsDB.DownloadColumns._ID)));
        d.setURL(c.getString(c.getColumnIndex(DownloadsDB.DownloadColumns.URL)));
        d.setTotal(c.getLong(c.getColumnIndex(DownloadsDB.DownloadColumns.DOWNLOADED)));
        d.setDownloaded(c.getLong(c.getColumnIndex(DownloadsDB.DownloadColumns.TOTAL)));
        return d;
    }

    public static void saveDownload(Context pContext, Uri pUri, Download pDownload) {
        ContentValues values = downloadToValues(pDownload);
        pContext.getContentResolver().update(pUri, values, null, null);
    }

    private static ContentValues downloadToValues(Download pDownload) {
        ContentValues values = new ContentValues();
        values.put(DownloadsDB.DownloadColumns.TOTAL, pDownload.getTotal());
        values.put(DownloadsDB.DownloadColumns.DOWNLOADED, pDownload.getDownloaded());
        values.put(DownloadsDB.DownloadColumns.DEST_PATH, pDownload.getDestFile());
        return values;
    }
}
