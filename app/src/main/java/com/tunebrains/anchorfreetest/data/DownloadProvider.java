package com.tunebrains.anchorfreetest.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

/**
 * Created by Alexandr Timoshenko <thick.tav@gmail.com> on 4/10/15.
 */
public class DownloadProvider extends ContentProvider {
    public static final int DOWNLOADS_CODE = 1;
    public static final int DOWNLOAD_CODE = 2;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sUriMatcher.addURI(DownloadsDB.AUTHORITY, "*/downloads", DOWNLOADS_CODE);
        sUriMatcher.addURI(DownloadsDB.AUTHORITY, "*/download/#", DOWNLOAD_CODE);
    }

    private DownloadsDB mDownloadsDB;


    @Override
    public boolean onCreate() {
        mDownloadsDB = new DownloadsDB(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri pUri, String[] projectionIn, String selection, String[] selectionArgs, String sortOrder) {
        int table = sUriMatcher.match(pUri);
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        switch (table) {
            case DOWNLOAD_CODE:
            case DOWNLOADS_CODE:
                qb.setTables("downloads");
                break;
        }
        Cursor c = qb.query(mDownloadsDB.getReadableDatabase(), projectionIn, selection,
                selectionArgs, null, null, sortOrder, null);
        c.setNotificationUri(getContext().getContentResolver(), pUri);
        return c;
    }

    @Override
    public String getType(Uri pUri) {
        switch (sUriMatcher.match(pUri)) {
            case DOWNLOADS_CODE:
            case DOWNLOAD_CODE:
                return "image/jpeg";
        }
        return null;
    }


    @Override
    public Uri insert(Uri pUri, ContentValues pContentValues) {
        int match = sUriMatcher.match(pUri);
        Uri insertedUri = null;
        switch (match) {
            case DOWNLOADS_CODE:
                long id = mDownloadsDB.getWritableDatabase().insertOrThrow("downloads", null, pContentValues);
                insertedUri = ContentUris.withAppendedId(DownloadsDB.DownloadColumns.
                        getItemUri(), id);
                break;
            case DOWNLOAD_CODE:
                throw new UnsupportedOperationException();
        }
        if (insertedUri != null) {
            getContext().getContentResolver().notifyChange(DownloadsDB.DownloadColumns.
                    getContentUri(), null);
            getContext().getContentResolver().notifyChange(insertedUri, null);
        }
        return insertedUri;
    }

    @Override
    public int delete(Uri pUri, String s, String[] pStrings) {
        int match = sUriMatcher.match(pUri);
        int result = 0;
        switch (match) {
            case DOWNLOADS_CODE:
                result = mDownloadsDB.getWritableDatabase().delete("downloads", s, pStrings);
                break;
            case DOWNLOAD_CODE:
                String pDownloadIDString = pUri.getLastPathSegment();
                result = mDownloadsDB.getWritableDatabase().delete("downloads", DownloadsDB.DownloadColumns._ID + "=?", new String[]{pDownloadIDString});
                break;
        }
        return result;
    }

    @Override
    public int update(Uri pUri, ContentValues pContentValues, String s, String[] pStrings) {
        int match = sUriMatcher.match(pUri);
        int result = 0;
        switch (match) {
            case DOWNLOADS_CODE:
                throw new UnsupportedOperationException();
            case DOWNLOAD_CODE:
                String pDownloadIDString = pUri.getLastPathSegment();
                result = mDownloadsDB.getWritableDatabase().update("downloads", pContentValues, DownloadsDB.DownloadColumns._ID + "=?", new String[]{pDownloadIDString});
                getContext().getContentResolver().notifyChange(DownloadsDB.DownloadColumns.
                        getContentUri(), null);

                break;
        }
        return result;
    }
}
