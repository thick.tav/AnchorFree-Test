package com.tunebrains.anchorfreetest.data;

/**
 * Created by Alexandr Timoshenko <thick.tav@gmail.com> on 4/10/15.
 */
public class Download {
    private long mID;
    private String mURL;
    private long mTotal;
    private long mDownloaded;
    private String mDestFile;
    private String mDownloadedFilePath;

    public Download() {
    }

    public long getID() {
        return mID;
    }

    public void setID(long pID) {
        mID = pID;
    }

    public String getURL() {
        return mURL;
    }

    public void setURL(String pURL) {
        mURL = pURL;
    }

    public long getTotal() {
        return mTotal;
    }

    public void setTotal(long pTotal) {
        mTotal = pTotal;
    }

    public long getDownloaded() {
        return mDownloaded;
    }

    public void setDownloaded(long pDownloaded) {
        mDownloaded = pDownloaded;
    }

    public String getDestFile() {
        return mDestFile;
    }

    public void setDestFile(String pDestFile) {
        mDestFile = pDestFile;
    }

    public String getDownloadedFilePath() {
        return mDownloadedFilePath;
    }

    public void setDownloadedFilePath(String pDownloadedFilePath) {
        mDownloadedFilePath = pDownloadedFilePath;
    }
}
