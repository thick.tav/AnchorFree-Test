package com.tunebrains.anchorfreetest.service;

import android.content.Context;
import android.content.Intent;

/**
 * Created by Alexandr Timoshenko <thick.tav@gmail.com> on 4/11/15.
 */
public class ServiceController {
    public static void start(Context pContext) {
        pContext.startService(new Intent(pContext, DownloadService.class));
    }

    public static void stop(Context pContext, boolean interrupt) {
        Intent stopIntent = new Intent(DownloadService.ACTION_CONTROL_SERVICE);
        if (interrupt) {
            stopIntent.putExtra(DownloadService.EXTRA_ACTION, DownloadService.ACTION_STOP_NOW);
        } else {
            stopIntent.putExtra(DownloadService.EXTRA_ACTION, DownloadService.ACTION_FINISH_ALL_AND_STOP);
        }
        pContext.sendBroadcast(stopIntent);
    }

}
