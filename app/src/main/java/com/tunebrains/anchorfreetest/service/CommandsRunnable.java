package com.tunebrains.anchorfreetest.service;

import android.content.Context;

import com.tunebrains.anchorfreetest.data.Download;
import com.tunebrains.anchorfreetest.logic.ICommand;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CommandsRunnable implements Runnable {
    private final Download mDownload;
    List<ICommand> mCommandList;
    private Context mContext;

    public CommandsRunnable(Context pContext, Collection<ICommand> pCommands, Download pDownload) {
        mDownload = pDownload;
        mContext = pContext;
        mCommandList = new ArrayList<>();
        mCommandList.addAll(pCommands);

    }


    @Override
    public void run() {
        for (ICommand lICommand : mCommandList) {
            if (!lICommand.handle(mContext, mDownload)) {
                lICommand.failed(mContext, mDownload);
                break;
            }
        }
    }
}