package com.tunebrains.anchorfreetest.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.tunebrains.anchorfreetest.R;
import com.tunebrains.anchorfreetest.data.Prefs;

/**
 * Created by Alexandr Timoshenko <thick.tav@gmail.com> on 4/10/15.
 */
public class DownloadService extends Service {

    public static final String ACTION_STATUS_UPDATE = "com.tunebrains.anchorfreetest.service_status_update";
    public static final String ACTION_CONTROL_SERVICE = "com.tunebrains.anchorfreetest.service_control";
    public static final String EXTRA_ACTION = "extra_action";
    public static final int ACTION_STOP_NOW = 1;
    public static final int ACTION_FINISH_ALL_AND_STOP = 2;

    public static final int NOTIFICATION_ID = 1000;
    private DownloadManager mDownloadManager;
    private Prefs mPrefs;
    private ControlReceiver mControlReceiver = new ControlReceiver();

    @Override
    public IBinder onBind(Intent pIntent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mDownloadManager = new DownloadManager(this);
        mPrefs = new Prefs(this);
        mPrefs.serviceStart();
        sendBroadcast(new Intent(ACTION_STATUS_UPDATE));
        showRunningNotification();
        IntentFilter controlFilter = new IntentFilter();
        controlFilter.addAction(ACTION_CONTROL_SERVICE);
        registerReceiver(mControlReceiver, controlFilter);

    }

    private void showRunningNotification() {
        NotificationCompat.Builder lBuilder = new NotificationCompat.Builder(this);
        lBuilder.setTicker(getString(R.string.download_service));
        startForeground(NOTIFICATION_ID, lBuilder.build());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mControlReceiver);
        stopForeground(true);
        mDownloadManager.stopNow();
        mPrefs.serviceStop();
        sendBroadcast(new Intent(ACTION_STATUS_UPDATE));
        super.onDestroy();
    }

    private class ControlReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context pContext, Intent pIntent) {
            String action = pIntent.getAction();
            if (ACTION_CONTROL_SERVICE.equals(action)) {
                handleControlAction(pIntent);
            }

        }

        private void handleControlAction(Intent pIntent) {
            int action = pIntent.getIntExtra(EXTRA_ACTION, 1);
            switch (action) {
                case ACTION_STOP_NOW:
                    stopSelf();
                    break;
                case ACTION_FINISH_ALL_AND_STOP:
                    Thread stop = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            mDownloadManager.stop();
                            stopSelf();
                        }
                    });
                    stop.start();
                    break;
            }

        }
    }

}
