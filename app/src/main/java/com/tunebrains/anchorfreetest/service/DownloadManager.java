package com.tunebrains.anchorfreetest.service;

import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;

import com.tunebrains.anchorfreetest.data.DBHelper;
import com.tunebrains.anchorfreetest.data.Download;
import com.tunebrains.anchorfreetest.data.DownloadsDB;
import com.tunebrains.anchorfreetest.logic.ICommand;
import com.tunebrains.anchorfreetest.logic.commands.LogicFactory;

import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Alexandr Timoshenko <thick.tav@gmail.com> on 4/10/15.
 */
public class DownloadManager extends ContentObserver {

    private final ExecutorService mExecutor;
    private Context mContext;

    public DownloadManager(Context pContext) {
        super(new Handler());
        mContext = pContext;
        mExecutor = Executors.newCachedThreadPool();
        registerNotifications();

    }


    private void registerNotifications() {
        mContext.getContentResolver().registerContentObserver(DownloadsDB.DownloadColumns.getItemUri(), true, this);
    }

    private void unRegisterNotifications() {
        mContext.getContentResolver().unregisterContentObserver(this);
    }


    public void stop() {
        mExecutor.shutdown();
        try {
            mExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mExecutor.shutdownNow();
    }

    public void stopNow() {
        unRegisterNotifications();
        mExecutor.shutdownNow();
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        super.onChange(selfChange, uri);
        Download d = DBHelper.getDownloadFromDB(mContext, uri);
        if (d != null) {
            scheduleDownload(LogicFactory.downloadAndPutToGallery(uri), d);
        }

    }

    private void scheduleDownload(Collection<ICommand> pCommands, Download pDownload) {
        mExecutor.submit(new CommandsRunnable(mContext, pCommands, pDownload));
    }


}
