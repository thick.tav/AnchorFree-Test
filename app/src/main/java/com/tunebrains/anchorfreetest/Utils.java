package com.tunebrains.anchorfreetest;

/**
 * Created by Alexandr Timoshenko <thick.tav@gmail.com> on 4/11/15.
 */
public class Utils {

    public static int percent(long total, long downloaded) {
        return total == 0 ? 0 : (int) (downloaded * 100 / total);
    }

    public static String fileName() {
        return String.format("%d.jpg", System.currentTimeMillis());
    }
}
