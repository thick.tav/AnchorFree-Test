package com.tunebrains.anchorfreetest.logic.commands;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Alexandr Timoshenko <thick.tav@gmail.com> on 4/11/15.
 */
class FileDownloader {
    public static void download(String url, String dest, DownloadProcessListener pDownloadProcessListener) throws IOException, InterruptedException {
        HttpURLConnection lHttpURLConnection = (HttpURLConnection) new URL(url).openConnection();
        int total = lHttpURLConnection.getContentLength();
        InputStream lInputStream = lHttpURLConnection.getInputStream();
        byte[] buf = new byte[1024 * 100];
        int len = 0;
        int downloaded = 0;
        File f = new File(dest);
        FileOutputStream fos = new FileOutputStream(f);
        boolean cancelled = false;
        while ((len = lInputStream.read(buf)) > 0 && !cancelled) {
            fos.write(buf, 0, len);
            downloaded += len;
            if (pDownloadProcessListener != null) {
                pDownloadProcessListener.onProcessUpdate(total, downloaded);
            }
            cancelled = Thread.currentThread().interrupted();
        }
        lHttpURLConnection.disconnect();
        fos.close();
        if (cancelled) {
            throw new InterruptedException();
        }

    }

    public static interface DownloadProcessListener {
        void onProcessUpdate(int total, int downloaded);
    }
}
