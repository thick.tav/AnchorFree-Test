package com.tunebrains.anchorfreetest.logic;

import android.content.Context;

import com.tunebrains.anchorfreetest.data.Download;

/**
 * Created by Alexandr Timoshenko <thick.tav@gmail.com> on 4/11/15.
 */
public interface ICommand {
    public boolean handle(Context pContext, Download pDownload);

    void failed(Context pContext, Download pDownload);
}
