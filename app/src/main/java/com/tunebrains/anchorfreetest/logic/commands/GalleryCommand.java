package com.tunebrains.anchorfreetest.logic.commands;

import android.content.Context;
import android.net.Uri;

import com.tunebrains.anchorfreetest.GalleryHelper;
import com.tunebrains.anchorfreetest.data.DBHelper;
import com.tunebrains.anchorfreetest.data.Download;
import com.tunebrains.anchorfreetest.logic.ICommand;

import java.io.File;

/**
 * Created by Alexandr Timoshenko <thick.tav@gmail.com> on 4/11/15.
 */
class GalleryCommand implements ICommand {
    private Uri mUri;

    public GalleryCommand(Uri pUri) {
        mUri = pUri;
    }

    @Override
    public boolean handle(Context pContext, Download pDownload) {
        Uri galleryUri = GalleryHelper.publishToGallery(pContext, pDownload.getDownloadedFilePath());
        boolean result = false;
        if (galleryUri != null) {
            pDownload.setDestFile(galleryUri.toString());

            File dest = new File(pDownload.getDownloadedFilePath());
            dest.delete();

            DBHelper.saveDownload(pContext, mUri, pDownload);
            result = true;
        }
        return result;

    }

    @Override
    public void failed(Context pContext, Download pDownload) {

    }
}
