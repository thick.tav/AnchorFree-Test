package com.tunebrains.anchorfreetest.logic.commands;

import android.net.Uri;

import com.tunebrains.anchorfreetest.logic.ICommand;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Alexandr Timoshenko <thick.tav@gmail.com> on 4/11/15.
 */
public class LogicFactory {

    public static Collection<ICommand> downloadAndPutToGallery(Uri pUri) {
        return Arrays.asList(new ICommand[]{new DownloadCommand(pUri), new GalleryCommand(pUri)});
    }
}
