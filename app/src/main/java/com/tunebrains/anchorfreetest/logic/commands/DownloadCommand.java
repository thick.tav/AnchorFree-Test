package com.tunebrains.anchorfreetest.logic.commands;

import android.content.Context;
import android.net.Uri;

import com.tunebrains.anchorfreetest.Utils;
import com.tunebrains.anchorfreetest.data.DBHelper;
import com.tunebrains.anchorfreetest.data.Download;
import com.tunebrains.anchorfreetest.logic.ICommand;

import java.io.File;
import java.io.IOException;

/**
 * Created by Alexandr Timoshenko <thick.tav@gmail.com> on 4/11/15.
 */
class DownloadCommand implements ICommand {
    private Uri mUri;

    DownloadCommand(Uri pUri) {
        mUri = pUri;
    }

    @Override
    public boolean handle(final Context pContext, final Download pDownload) {
        File dest = new File(pContext.getCacheDir(), Utils.fileName());
        boolean result = false;
        try {
            String source = pDownload.getURL();
            FileDownloader.download(source, dest.getAbsolutePath(), new ProcessListener(pDownload, pContext, mUri));
            pDownload.setDownloadedFilePath(dest.getAbsolutePath());
            DBHelper.saveDownload(pContext, mUri, pDownload);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void failed(Context pContext, Download pDownload) {
        //can set failed status of download here
    }

    private static class ProcessListener implements FileDownloader.DownloadProcessListener {

        private int mLastPercent = 0;
        private Download mDownload;
        private Context mContext;
        private Uri mUri;

        private ProcessListener(Download pDownload, Context pContext, Uri pUri) {
            mDownload = pDownload;
            mContext = pContext;
            mUri = pUri;
        }

        @Override
        public void onProcessUpdate(int total, int downloaded) {
            int currentPercent = Utils.percent(total, downloaded);

            if (currentPercent > mLastPercent) {
                mDownload.setDownloaded(downloaded);
                mDownload.setTotal(total);
                DBHelper.saveDownload(mContext, mUri, mDownload);
                mLastPercent = currentPercent;
            }
        }
    }
}
