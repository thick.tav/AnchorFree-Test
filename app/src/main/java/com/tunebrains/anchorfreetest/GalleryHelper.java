package com.tunebrains.anchorfreetest;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Alexandr Timoshenko <thick.tav@gmail.com> on 4/11/15.
 */
public class GalleryHelper {

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private static Bitmap getResizedBitmap(String pAbsolutePath) {
        BitmapFactory.Options lOptions = new BitmapFactory.Options();
        lOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pAbsolutePath, lOptions);
        //target size 320x480
        lOptions.inSampleSize = calculateInSampleSize(lOptions, 320, 480);
        lOptions.inJustDecodeBounds = false;

        Bitmap sizeBitmap = BitmapFactory.decodeFile(pAbsolutePath, lOptions);


        Bitmap result = Bitmap.createScaledBitmap(sizeBitmap, 320, 480, true);
        sizeBitmap.recycle();
        return result;
    }

    public static Uri publishToGallery(Context pContext, String pAbsolutePath) {

        try {
            Bitmap bmp = getResizedBitmap(pAbsolutePath);

            File outFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), Utils.fileName());
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(outFile));

            ContentValues values = new ContentValues();

            values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
            values.put(MediaStore.MediaColumns.DATA, outFile.getAbsolutePath());

            Uri result = pContext.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
}
