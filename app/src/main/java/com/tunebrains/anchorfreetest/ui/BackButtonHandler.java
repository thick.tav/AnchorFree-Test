package com.tunebrains.anchorfreetest.ui;

import android.app.Activity;
import android.widget.Toast;

import com.tunebrains.anchorfreetest.service.ServiceController;

import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Alexandr Timoshenko <thick.tav@gmail.com> on 4/11/15.
 */
public class BackButtonHandler {

    private WeakReference<Activity> mActivity;
    private int mBackCounter;
    private Timer mBackTimer;

    public BackButtonHandler(Activity pActivity) {

        mActivity = new WeakReference<>(pActivity);
        mBackCounter = 0;
        mBackTimer = new Timer();

    }

    public void handle() {

        mBackCounter++;
        if (mBackCounter >= 2) {
            if (mActivity.get() != null) {
                ServiceController.stop(mActivity.get(), false);
                mActivity.get().finish();
            }
            return;
        } else {
            Toast.makeText(mActivity.get(), "Press second time to finish service when all download complete", Toast.LENGTH_LONG).show();
        }
        mBackTimer.cancel();
        mBackTimer = new Timer();
        mBackTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mActivity.get() != null) {
                    mActivity.get().finish();
                }
            }
        }, 1000);
    }

    public void clear() {
        mActivity.clear();
        mBackTimer.cancel();
        mBackTimer = null;
    }
}
