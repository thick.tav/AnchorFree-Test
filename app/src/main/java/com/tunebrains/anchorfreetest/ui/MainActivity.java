package com.tunebrains.anchorfreetest.ui;

import android.app.LoaderManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.tunebrains.anchorfreetest.R;
import com.tunebrains.anchorfreetest.data.DBHelper;
import com.tunebrains.anchorfreetest.data.DownloadsDB;
import com.tunebrains.anchorfreetest.data.Prefs;
import com.tunebrains.anchorfreetest.service.DownloadService;
import com.tunebrains.anchorfreetest.service.ServiceController;

import java.lang.ref.WeakReference;


public class MainActivity extends ActionBarActivity {

    private ListView mDownloadsList;
    private DownloadsAdapter mDownloadsAdapter;
    private EditText mPhotoUrl;
    private Prefs mPrefs;
    private StatusUpdateReceiver mStatusUpdateReceiver;
    private BackButtonHandler mBackButtonHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mStatusUpdateReceiver = new StatusUpdateReceiver();
        mBackButtonHandler = new BackButtonHandler(this);
        mPrefs = new Prefs(getApplicationContext());

        findViewById(R.id.add_to_queue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View pView) {
                addToQueue();
            }
        });

        mDownloadsList = (ListView) findViewById(R.id.queue);
        mDownloadsAdapter = new DownloadsAdapter(this, null, true);
        mDownloadsList.setAdapter(mDownloadsAdapter);
        getLoaderManager().initLoader(0, null, new LoaderManagerListener(getApplicationContext()));

        mPhotoUrl = (EditText) findViewById(R.id.photo_url);
        mPhotoUrl.setText("http://edmullen.net/test/rc.jpg");

        ServiceController.start(this);
    }

    private void addToQueue() {
        String url = mPhotoUrl.getText().toString();
        mPhotoUrl.setError(null);

        if (!TextUtils.isEmpty(url)) {
            DBHelper.addDownload(this, url);
        } else {
            mPhotoUrl.setError(getString(R.string.url_cannot_be_empty));
        }
    }

    @Override
    protected void onPause() {
        unregisterReceiver(mStatusUpdateReceiver);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean serviceWorking = mPrefs.isServiceWorking();
        menu.findItem(R.id.start_service).setEnabled(!serviceWorking);
        menu.findItem(R.id.stop_service).setEnabled(serviceWorking);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = false;
        switch (item.getItemId()) {
            case R.id.stop_service:
                ServiceController.stop(this, true);
                handled = true;
                break;
            case R.id.start_service:
                ServiceController.start(this);
                handled = true;
                break;
        }
        invalidateOptionsMenu();
        return handled;
    }

    @Override
    public void onBackPressed() {
        mBackButtonHandler.handle();
    }

    @Override
    protected void onDestroy() {
        mStatusUpdateReceiver = null;
        mBackButtonHandler.clear();
        mBackButtonHandler = null;
        mPrefs.clear();
        mPrefs = null;
        getLoaderManager().destroyLoader(0);
        mDownloadsList.setAdapter(null);
        mDownloadsList = null;
        mDownloadsAdapter.clear();
        mDownloadsAdapter.changeCursor(null);
        mDownloadsAdapter = null;
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter statusFilter = new IntentFilter();
        statusFilter.addAction(DownloadService.ACTION_STATUS_UPDATE);
        registerReceiver(mStatusUpdateReceiver, statusFilter);
    }

    private class LoaderManagerListener implements LoaderManager.LoaderCallbacks<Cursor> {
        private WeakReference<Context> mContext;

        private LoaderManagerListener(Context pContext) {
            mContext = new WeakReference<>(pContext);
        }

        @Override
        public Loader<Cursor> onCreateLoader(int i, Bundle pBundle) {
            return new CursorLoader(mContext.get(), DownloadsDB.DownloadColumns.getContentUri(), null, null, null, null);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> pObjectLoader, Cursor o) {
            mDownloadsAdapter.changeCursor(o);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> pObjectLoader) {
            mDownloadsAdapter.changeCursor(null);
        }
    }

    private class StatusUpdateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context pContext, Intent pIntent) {
            String action = pIntent.getAction();
            if (DownloadService.ACTION_STATUS_UPDATE.equals(action)) {
                invalidateOptionsMenu();
            }
        }
    }
}
