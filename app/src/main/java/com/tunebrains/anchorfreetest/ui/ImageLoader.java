package com.tunebrains.anchorfreetest.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.LruCache;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

/**
 * Created by Alexandr Timoshenko <thick.tav@gmail.com> on 4/10/15.
 */
public class ImageLoader {
    private final LruCache<String, Bitmap> mLruCache;
    private Context mContext;
    private Handler mHandler;


    public ImageLoader(Context pContext) {
        mContext = pContext;
        mLruCache = new LruCache<>(10);
        mHandler = new Handler();
    }

    public void loadBitmap(final ImageView pImageView, final String uri) {
        Bitmap cached = mLruCache.get(uri);
        final WeakReference<ImageView> lImageViewWeakReference = new WeakReference<>(pImageView);
        if (cached == null) {
            Thread k = new Thread(new Runnable() {
                @Override
                public void run() {
                    Uri imUri = Uri.parse(uri);
                    final Bitmap bmp = MediaStore.Images.Thumbnails.getThumbnail(mContext.getContentResolver(), Long.valueOf(imUri.getLastPathSegment()), MediaStore.Images.Thumbnails.MICRO_KIND, null);
                    mLruCache.put(uri, bmp);

                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (lImageViewWeakReference.get() != null) {
                                lImageViewWeakReference.get().setImageBitmap(bmp);
                            }
                        }
                    });
                }
            });
            k.start();

        } else {
            lImageViewWeakReference.get().setImageBitmap(cached);
        }

    }

    public void clear() {
        mLruCache.evictAll();

        mHandler = null;
        mContext = null;
    }
}
