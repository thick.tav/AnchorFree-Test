package com.tunebrains.anchorfreetest.ui;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tunebrains.anchorfreetest.R;
import com.tunebrains.anchorfreetest.Utils;
import com.tunebrains.anchorfreetest.data.DownloadsDB;

class DownloadsAdapter extends CursorAdapter {
    private final ImageLoader mImageLoader;

    private final LayoutInflater mLayoutInflater;

    public DownloadsAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        mImageLoader = new ImageLoader(context);
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context pContext, Cursor pCursor, ViewGroup pViewGroup) {
        View contentView = mLayoutInflater.inflate(R.layout.download_item, null);
        return contentView;
    }

    @Override
    public void bindView(View pView, Context pContext, Cursor pCursor) {
        TextView downloadUrl = (TextView) pView.findViewById(R.id.url);
        downloadUrl.setText(pCursor.getString(pCursor.getColumnIndex(DownloadsDB.DownloadColumns.URL)));

        TextView downloadPercent = (TextView) pView.findViewById(R.id.download_percent);
        downloadPercent.setText(calcDownloadPercent(pCursor));

        ImageView preview = (ImageView) pView.findViewById(R.id.preview);
        setPreview(mImageLoader, preview, pCursor);
    }

    public String calcDownloadPercent(Cursor pCursor) {
        long total = pCursor.getLong(pCursor.getColumnIndex(DownloadsDB.DownloadColumns.TOTAL));
        long downloaded = pCursor.getLong(pCursor.getColumnIndex(DownloadsDB.DownloadColumns.DOWNLOADED));

        return String.format("%d%%", Utils.percent(total, downloaded));
    }

    private void setPreview(ImageLoader pImageLoader, ImageView pPreview, Cursor pCursor) {
        String path = pCursor.getString(pCursor.getColumnIndex(DownloadsDB.DownloadColumns.DEST_PATH));
        if (TextUtils.isEmpty(path)) {
            pPreview.setImageBitmap(null);
        } else {
            pImageLoader.loadBitmap(pPreview, path);
        }
    }

    public void clear() {
        mImageLoader.clear();
    }
}